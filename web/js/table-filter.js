$(document).ready(function () {

    $("#filter").keyup(function () {
        filter = new RegExp($(this).val(), 'i');
        $("#users-table tbody tr").filter(function () {
            $(this).each(function () {
                found = false;
                $(this).children(':eq(0)').each(function () {
                    content = $(this).html();
                    if (content.match(filter)) {
                        found = true
                    }
                });
                if (!found) {
                    $(this).hide();
                }
                else {
                    $(this).show();
                }
            });
        }).show();
    });
});
