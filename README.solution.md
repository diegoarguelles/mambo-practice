#Soluciòn

## Backend

El sistema va a realizar la lectura del archivo "users.csv" al ejecutar el siguiente comando:

`php import.php`

Considerando que el numero de operaciones de lectura/escritura pueden variar, se puede agregar un argumento extra definiendo el numero de filas que deseamos leer por bloque (por defecto esta configurado a 10):

`php import.php <numero-de-filas>`

Las inserciones a base de datos se realizan en una sola query por cada bloque de lectura.


## Frontend

- Paginación. Divide los resultados en páginas, cada página debe contener 10 filas. 
  _Optimiza enormemente el consumo de datos y el tiempo de carga de los mismos._
  
  
- Filtrar la tabla por nombre. Implementa una caja de texto que permita filtrar la tabla por la columna nombre.
  _Permite al usuario encontrar de manera rapida un registro en especifico segun ciertos parametros de busqueda, sin necesidad de ir leyendo uno por uno_

- Ordenamiento de columnas. Escribe el código necesario para ordenar cualquier columna de forma ascendente y descendente.
  _Similar al anterior, permite al usuario ordenar la informacion de manera rapida segun como crea conveniente_

- Encabezado Fijo. Escribe el código necesario para que al hacer scroll en la página, el encabezado de la tabla permanezca visible todo el tiempo en la parte superior de la página.
  _Ayuda a tener mapeado el nombre de cada columna en caso se deba hacer scroll en la pagina y el numero de registros sea alto._

- Avatar en modal. Implementa un mecanismo que permita que al hacer clic en el avatar de un usuario, este se muestre en su tamaño original en una ventana modal.
  _Permite observar mejor las caracteristicas de los usuarios en caso se desee revisar el avatar de alguno en especifico._
  
  
