<?php
/**
 * Created by IntelliJ IDEA.
 * User: Diego Arguelles
 * Date: 11/15/17
 * Time: 1:08 a.m.
 */
require __CLASS__.'Config.php';


class SQLiteConnection
{

    private $pdo;

    public function connect() {
        if ($this->pdo == null) {
            $this->pdo = new \PDO("sqlite:" . Config::PATH_TO_SQLITE_FILE);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return $this->pdo;
    }

}