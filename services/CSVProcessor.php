<?php
/**
 * Created by IntelliJ IDEA.
 * User: Diego Arguelles
 * Date: 11/14/17
 * Time: 8:48 p.m.
 */

class CSVProcessor
{

    private $file_processor;
    private $parse_header;
    private $delimiter;
    private $length;

    function __construct($file_name, $parse_header=false, $delimiter="\t", $length=8000)
    {
        $this->file_processor = fopen($file_name, "r");
        $this->parse_header = $parse_header;
        $this->delimiter = $delimiter;
        $this->length = $length;

        if ($this->parse_header)
        {
            $this->header = fgetcsv($this->file_processor, $this->length, $this->delimiter);
        }
    }

    function __destruct()
    {
        if ($this->file_processor)
        {
            fclose($this->file_processor);
        }
    }

    function getRows($max_lines=0)
    {

        $data = array();

        if ($max_lines > 0)
            $line_count = 0;
        else
            $line_count = -1;

        while ($line_count < $max_lines && ($row = fgetcsv($this->file_processor, $this->length, $this->delimiter)) !== FALSE)
        {
            if ($this->parse_header)
            {
                foreach ($this->header as $i => $heading_i)
                {
                    $row_new[$heading_i] = $row[$i];
                }
                $data[] = $row_new;
            }
            else
            {
                $data[] = $row;
            }

            if ($max_lines > 0)
                $line_count++;
        }
        return $data;
    }

}