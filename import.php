<?php
/**
 * Crea las clases que creas necesarias para hacer el trabajo.
 * Ten en cuenta que el archivo `users.csv`
 */

require __CLASS__ . 'config/UserRepository.php';
require __CLASS__ . 'services/CSVProcessor.php';

$importer = new CSVProcessor("users.csv", true);
$repository = new UserRepository();

$rows_number = var_dump($argv[1]);

while ($rows_imported = $importer->getRows($rows_number = 10)) {

    $insert_values = array();
    $insert_marks = array();

    $query_marks = '(' . setParamPlaceholders('?', 6) . ')';

    foreach ($rows_imported as $user_row) {

        $user_row_string = implode('&', $user_row);

        $user_data = explode('|', $user_row_string);

        $user_name = $user_data[0];
        $user_lastname = $user_data[1];
        $user_address = $user_data[2];
        $user_tel = explode('-', $user_data[3])[0];
        $user_cell = explode('-', $user_data[3])[1];
        $user_pic = $user_data[4];

        $user = array($user_name, $user_lastname, $user_address, $user_tel, $user_cell, $user_pic);

        $insert_values = array_merge($insert_values, array_values($user));
        array_push($insert_marks, '(' . setParamPlaceholders('?', 6) . ')');

    }

    $repository->save($insert_marks, $insert_values);

}

function setParamPlaceholders($text, $count = 0, $separator = ",")
{
    $result = array();
    if ($count > 0) {
        for ($x = 0; $x < $count; $x++) {
            $result[] = $text;
        }
    }
    return implode($separator, $result);
}
