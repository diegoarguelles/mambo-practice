<?php
/**
 * Created by IntelliJ IDEA.
 * User: Diego Arguelles
 * Date: 11/15/17
 * Time: 12:43 a.m.
 */

require __CLASS__ . 'SQLiteConnection.php';

class UserRepository
{

    private $pdo;

    function save($marks, $data)
    {

        $sql = "INSERT INTO users ('name', 'last_name', 'address' , 'telephone' , 'cellphone' , 'avatar') VALUES " . implode(',', $marks);

        $pdo = (new SQLiteConnection())->connect();
        $pdo->beginTransaction();

        $stmt = $pdo->prepare($sql);

        try {
            $stmt->execute($data);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        $pdo->commit();

        $pdo = null;
    }

}



